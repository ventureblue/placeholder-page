Energizer Placeholder Landing Page
==================================
---
Basic mobile-responsive placeholder page for Energizer, pointing people to either visit the European Energizer site or email us for order queries.